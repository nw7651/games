class Board:
	"""This is the main game board
	it keeps track of which spaces on the board are occupied
	and prints the board showing which spaces are occupied

	the default size is 3x3"""
	def __init__(self,x=3,y=3):
		self.x = x
		self.y = y
		self.p1Spaces = [False]
		self.p2Spaces = [False]
		size = x * y

		while size > 0:
			self.p1Spaces.append(False)
			self.p2Spaces.append(False)
			size = size - 1

			pass

	def printBoard(self):

		x_size = self.x + 1
		y_size = self.y 
		p1Spaces = self.p1Spaces
		p2Spaces = self.p2Spaces
		currentSpace = 0
		def printX(self,p1Spaces,p2Spaces,here):
			x_size = self.x
			piSpaceOccupied = p1Spaces[here]
			p2SpaceOccupied = p2Spaces[here]

			print("|", end="")
			while x_size > 0:
				print("-----|",end="")
				x_size = x_size - 1
				pass
			x_size = self.x
			print()

			while x_size > 0:
				if piSpaceOccupied == True :

					print("|  X  ",end="")

				elif p2SpaceOccupied == True:

					print("|  O  ",end="")
				else:

					print("|  " + str(here) + "  ",end="")

				x_size = x_size - 1
				here = here + 1
				piSpaceOccupied = p1Spaces[here]
				p2SpaceOccupied = p2Spaces[here]
				
			pass

			print("|")

			return here

			print()
		while y_size > 0 :
			currentSpace = printX(self,p1Spaces,p2Spaces,currentSpace)
			y_size = y_size - 1
			pass
		print("|", end="")
		x_size = self.x
		while x_size > 0:
			print("-----|",end="")
			x_size = x_size - 1
			pass
		print()
	"""This function occupies a space based on the number given when called
	The numbers start from the top left at number 0 and moves left to right
	with each row being the multiple of the total number of columns

	"""
	def occupyP1Space(self,spaceNumber):
		if self.p2Spaces[spaceNumber]  or self.p1Spaces[spaceNumber] :
			print("This space is already occupied")
			return False
		else:
			self.p1Spaces[spaceNumber] = True

		pass
	"""Clears an occupied space using the same formating as above

	"""
	def unOccupyP1Space(self,spaceNumber):
		self.p1Spaces[spaceNumber] = False

		pass

	def occupyP2Space(self,spaceNumber):
		if self.p1Spaces[spaceNumber] or self.p2Spaces[spaceNumber] :
			return False
		else:
			self.p2Spaces[spaceNumber] = True

		pass

	def unOccupyP2Space(self,spaceNumber):
		self.p2Spaces[spaceNumber] = False

		pass

# Check if the game is won
	def isWon(self):
		score = 0
		size = self.x * self.y
		"""Check if the even spaces are occupied
		If at least 3 even spaces are taken by the same player, the game is won
		"""
		if checkDiagnal(self.p1Spaces) :
			return True
		elif checkDiagnal(self.p2Spaces) :
			return True

		"""Next check if the pattern even odd even is present
		"""
		if checkSides(self.p1Spaces) :
			return True
		elif checkSides(self.p2Spaces) :
			return True
		return False

def checkSides(spaces) :
	return (spaces[0] and spaces[1] and spaces[2]
	 or spaces[0] and spaces[3] and spaces[6]
	 or spaces[6] and spaces[7] and spaces[8]
	 or spaces[2] and spaces[5] and spaces[8]
	 or spaces[3] and spaces[4] and spaces[5])

def checkDiagnal(spaces, size=9) :
	return (spaces[0] and spaces[4] and spaces[8]
	or spaces[2] and spaces[4] and spaces[6])			