import tick_lib
import random
gameRunning = True
board = tick_lib.Board()

print("welcome to my tic tak toe game!")
board.printBoard()

while gameRunning :

	moveValid = False
	while moveValid == False:
		print("select the number for the space you want to go: ")

		playerMove = int(input())
		moveValid = board.occupyP1Space(playerMove)

	if board.isWon() :
		print("Player Wins")
		gameRunning = False
		board.printBoard()
		break

	board.printBoard()
	print()
	print()

	
	moveValid = False
	while moveValid == False:
		computerMove = random.randint(0, 8)
		moveValid = board.occupyP2Space(computerMove)

	board.printBoard()

	if board.isWon() :
		print("Computer wins")
		gameRunning = False